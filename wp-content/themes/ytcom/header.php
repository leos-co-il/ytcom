<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
	<script>
		const timerStart = Date.now();
	</script>
	<div class="debug bg-danger border">
		<p class="width">
			<span>Width:</span>
			<span class="val"></span>
		</p>
		<p class="height">
			<span>Height:</span>
			<span class="val"></span>
		</p>
		<p class="media-query">
			<span>Media Query:</span>
			<span class="val"></span>
		</p>
		<p class="zoom">
			<span>Zoom:</span>
			<span class="val"></span>
		</p>
		<p class="dom-ready">
			<span>DOM Ready:</span>
			<span class="val"></span>
		</p>
		<p class="load-time">
			<span>Loading Time:</span>
			<span class="val"></span>
		</p>
	</div>
<?php endif; ?>


<header>
	<div class="container-fluid">
		<div class="row align-items-md-end align-items-center justify-content-between position-relative">
			<div class="col-md col-6">
				<div class="row align-items-end row-column-custom">
					<div class="col">
						<div class="row">
							<div class="col-12 d-flex justify-content-start header-socials-col">
								<div class="header-socials">
									<?php if ($youtube = opt('youtube')) : ?>
										<a href="<?= $youtube; ?>" class="social-item">
											<img src="<?= ICONS ?>youtube.png" alt="youtube">
										</a>
									<?php endif;
									if ($facebook = opt('facebook')) : ?>
										<a href="<?= $facebook; ?>" class="social-item">
											<img src="<?= ICONS ?>facebook.png" alt="facebook">
										</a>
									<?php endif; ?>
									<?php if ($linkedin = opt('facebook')) : ?>
										<a href="<?= $linkedin; ?>" class="social-item">
											<img src="<?= ICONS ?>linkedin.png" alt="linkedin">
										</a>
									<?php endif; ?>
								</div>
                                <div class="langs-wrap">
                                    <?php site_languages(); ?>
                                </div>
							</div>
							<?php if ($tel = opt('tel')) : ?>
								<div class="col-12 tel-col">
									<a href="tel:<?= $tel; ?>" class="header-tel">
										<span class="header-tel-number"><?= $tel; ?></span>
                                        <span class="header-tel-icon-wrap">
                                            <img src="<?= ICONS ?>header-tel.png" alt="tel">
                                        </span>
									</a>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-auto user-cart-wrap pb-menu">
						<a href="<?= wc_get_cart_url(); ?>" class="header-btn" id="mini-cart">
							<img src="<?= ICONS ?>basket.png" alt="shopping-cart">
							<span class="count-circle" id="cart-count">
								<?= WC()->cart->get_cart_contents_count(); ?>
							</span>
						</a>
						<a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
						   class="login-link-header">
							<img src="<?= ICONS ?>user.png" alt="user">
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-auto col-1 pb-menu">
				<nav id="MainNav" class="h-100">
					<div id="MobNavBtn">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
				</nav>
			</div>
			<div class="col-md col-5 pb-menu">
				<?php if ($logo = opt('logo')) : ?>
					<div class="header-logo-col">
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-7 col-md-8 col-12 d-flex justify-content-center">
				<div class="float-form">
					<?php get_template_part('views/partials/repeat', 'form', [
							'title' => opt('pop_form_title'),
							'subtitle' => opt('pop_form_subtitle'),
							'id' => '52',
					]); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="triggers-col">
	<div class="trigger-item pop-trigger">
		<img src="<?= ICONS ?>pop-trigger.png" alt="popup">
	</div>
	<?php if ($whatsapp = opt('whatsapp')) : ?>
		<a href=https://api.whatsapp.com/send?phone="<?= $whatsapp; ?>" class="trigger-item">
			<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp">
		</a>
	<?php endif; ?>
</div>
