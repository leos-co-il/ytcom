(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$('.menu-item-has-children').hover(function(){
		$(this).children('.sub-menu').fadeIn();
	}, function(){
		$(this).children('.sub-menu').fadeOut();
	});
	function addToCartAnimation(button){
		var target        = $('#mini-cart'),
			target_offset = target.offset();

		var target_x = target_offset.left,
			target_y = target_offset.top;

		var obj_id = 1 + Math.floor(Math.random() * 100000),
			obj_class = 'cart-animation-helper',
			obj_class_id = obj_class + '_' + obj_id;

		var obj = $("<div>", {
			'class': obj_class + ' ' + obj_class_id
		});

		button.parent().parent().append(obj);

		var obj_offset = obj.offset(),
			dist_x = target_x - obj_offset.left + 10,
			dist_y = target_y - obj_offset.top + 10,
			delay  = 0.8; // seconds

		setTimeout(function(){
			obj.css({
				'transition': 'transform ' + delay + 's ease-in',
				'transform' : "translateX(" + dist_x + "px)"
			});
			$('<style>.' + obj_class_id + ':after{ \
				transform: translateY(' + dist_y + 'px); \
				opacity: 1; \
				z-index: 99999999999; \
				border-radius: 100%; \
				height: 20px; \
				width: 20px; margin: 0; \
			}</style>').appendTo('head');
		}, 0);


		obj.show(1).delay((delay + 0.02) * 1000).hide(1, function() {
			$(obj).remove();
		});
	}
	function update_cart_count(){
		var count = $('#cart-count');
		count.html('<i class="far fa-smile-beam fa-spin" style="color: #fff"></i>');

		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'POST',
			data: {
				action: 'update_cart_count',
			},
			success: function (results) {
				count.html(results/10);
			}
		});

	}
	$( document ).ready(function() {
		function intToFloat(num, decPlaces) { return num.toFixed(decPlaces); }
		$(document).on('click', '.plus, .minus', function () {

			var $_class = $(this).hasClass('mini-cart-ctrl') ? '.mini-cart-qty-for-' : '.qty-for-';
			var qty =  $($_class + $(this).data('id'));
			var val = parseFloat(qty.val());
			var max = 9999;
			var min = 1;
			var step = 1;
			if ($(this).is('.plus')) {
				if (max && (max <= val)) {
					qty.val(max);
				} else {
					qty.val(val + step);
				}
			} else {
				if (min && (min >= val)) {
					qty.val(min);
				} else if (val > 1) {
					qty.val(val - step);
				}
			}
			var ret = qty.val();
			var sale = $('.price-final-sale');
			var priceSale = sale.data('price_sale');
			var final = $('.price-final');
			var symbol = final.data('sym');
			var priceFinal = final.data('price');
			var priceReg = intToFloat(ret * priceFinal, 2);
			var priceSaleTotal = intToFloat(ret * priceSale, 2);
			sale.html(priceSaleTotal+symbol);
			final.html(priceReg+symbol);

			var buyLink = $('.buy-now-custom');
			var siteUrl = buyLink.data('url');
			var prodId = buyLink.data('id');
			var finalLink = siteUrl+'/checkout/?add-to-cart='+prodId+'&quantity='+qty.val();
			buyLink.attr('href', finalLink);
			console.log(buyLink);
		});
		$('.single_add_to_cart_button').click(function (e) {
			// e.preventDefault();
			addToCartAnimation($(this));
			var cartEl = $('.mini-cart-wrap');
			cartEl.html('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');


			var btn = $(this);
			btn.addClass('loading');
			update_cart_count();


		});

		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.worker-trigger').click(function() {
			$(this).parent('.worker-item').children('.worker-content-appear').fadeIn();
			$(this).addClass('hide');
			$(this).parent('.worker-item').children('.worker-close').addClass('show');
		});
		$('.worker-close').click(function() {
			$(this).parent('.worker-item').children('.worker-content-appear').fadeOut();
			$(this).parent('.worker-item').children('.worker-trigger').removeClass('hide');
			$(this).removeClass('show');
		});
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.search-trigger').click(function () {
			$('.pop-search').addClass('show-search');
			$('.float-search').addClass('show-float-search');
		});
		$('.close-search').click(function () {
			$('.pop-search').removeClass('show-search');
			$('.float-search').removeClass('show-float-search');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 2,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.gallery-slider',
			dots: false,
			arrows: false,
		});
		$('.product-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1400,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 500,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.partners-slider').slick({
			slidesToShow: 7,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 6,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-post').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		$('#get-details').click(function () {
			var idProdHidden = $('.prod-form-wrap').data('id');
			var linkProdHidden = $('.prod-form-wrap').data('link');
			$('#hidden-name').html(idProdHidden);
			$('#hidden-link').html(linkProdHidden);
		});
	});

	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading mx-2"><i class="fas fa-spinner fa-pulse"></i></div>');
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var type = $(this).data('type');
		var count = $(this).data('count');
		var page = $(this).data('page');
		var quantity = $('.more-card').length;

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				termID: termID,
				ids: ids,
				count: count,
				quantity : quantity,
				type: type,
				page: page,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
	//More products
	var button = $( '#loadmore a' );
	var paged = button.data( 'paged' );
	var	maxPages = button.data( 'maxpages' );
	var textLoad = button.data('loading');
	var textLoadMore = button.data('load');

	button.click( function( event ) {

		event.preventDefault();
		var ids = '';
		$('.more-prod').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		$.ajax({
			type : 'POST',
			dataType: 'json',
			url: '/wp-admin/admin-ajax.php',
			data : {
				paged : paged,
				ids: ids,
				action : 'loadmore'
			},
			beforeSend : function() {
				button.text(textLoad);
			},
			success : function( data ){

				paged++;
				$('.put-here-prods').append(data.html);
				button.text(textLoadMore);

				if( paged === maxPages ) {
					button.remove();
				}
				if (!data.html) {
					$('.more-link').addClass('hide');
				}
			}

		});

	} );
})( jQuery );
