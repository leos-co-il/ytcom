<?php

get_header();
$fields = get_fields();
$query = get_queried_object();
$posts = new WP_Query([
    'posts_per_page' => 6,
    'post_type' => 'post',
    'suppress_filters' => false,
    'tax_query' => [
        [
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $query->term_id,
        ]
    ]
]);
$published_posts = count(get_posts([
    'numberposts' => -1,
    'post_type' => 'post',
    'suppress_filters' => false,
    'tax_query' => [
        [
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $query->term_id,
        ]
    ]
]));
?>

<article class="article-page-body page-body">
    <?php get_template_part('views/partials/content', 'page_top',
        [
            'slider' => get_field('page_slider', $query),
            'top_content' => get_field('top_content', $query),
        ]); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1 class="base-title"><?= $query->name; ?></h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-9 col-md-10 col-12">
                <div class="base-output text-center">
                    <?= category_description(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="cats-body-back">
        <?php if ($posts->have_posts()) : ?>
            <div class="container">
                <div class="row justify-content-center align-items-stretch put-here-posts">
                    <?php foreach ($posts->posts as $i => $post) {
                        get_template_part('views/partials/card', 'post', [
                            'post' => $post,
                        ]);
                    } ?>
                </div>
            </div>
        <?php endif;
        if ($published_posts && $published_posts > 6) : ?>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-auto">
                        <div class="more-link load-more-posts" data-type="post" data-term="<?= $query->term_id; ?>">
                            <?= lang_text(['he' => 'טען עוד מאמרים', 'en' => 'Load more posts'], 'he'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</article>
<?php
get_template_part('views/partials/repeat', 'reviews',
    [
        'content' => get_field('review_item', $query),
        'title' => get_field('reviews_title', $query),
    ]);
get_template_part('views/partials/repeat', 'banner',
    [
        'content' => get_field('banner_text', $query),
        'img' => get_field('banner_img', $query),
    ]);
if ($slider = get_field('single_slider_seo', $query)) {
    get_template_part('views/partials/content', 'slider',
        [
            'content' => $slider,
            'img' => get_field('slider_img', $query),
        ]);
}
get_footer(); ?>
