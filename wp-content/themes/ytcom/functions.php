<?php
defined('ABSPATH') or die("No script kiddies please!");
//require_once('inc/bs-walker.php');
require_once 'inc/helper.php';
define('THEMEDIR', get_template_directory_uri());
define('THEMEPATH', get_template_directory());
define('PARTIALS', THEMEPATH . '/views/partials/');
define('STYLES', THEMEDIR . '/assets/styles/');
define('SCRIPTS', THEMEDIR . '/assets/scripts/');
define('IMG', THEMEDIR . '/assets/img/');
define('ICONS', THEMEDIR . '/assets/iconsall/');
define('PLUGINS', THEMEDIR . '/assets/plugins/');
define('NODE', THEMEDIR . '/node_modules/');
define('CS_PAGE_ASSETS', THEMEDIR . '/assets/coming-soon/');
define('CATALOG', false);
define('PROJECTS', false);
define('ENV', get_option('stage'));
define('TEXTDOMAIN', 'leos');
define('LANG', get_lang());


// ******* SETUP *******
load_theme_textdomain(TEXTDOMAIN, get_template_directory() . '/languages');

add_filter('show_admin_bar', '__return_false');

function ytcom_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'ytcom_add_woocommerce_support' );

function disable_wp_emojicons($plugins){
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

function filter_site_upload_size_limit( $size ) {
    $size = 1024 * 10000;
    return $size;
}

add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 20 );

function wpcontent_svg_mime_type( $mimes = array() ) {
    $mimes['svg']  = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'wpcontent_svg_mime_type' );

add_action('init', 'disable_wp_emojicons');

//Images sizes
add_image_size('gallery-thumb', 134, 94);


function scriptsInit()
{

    wp_enqueue_style('bootstrap', STYLES . 'bootstrap_app.css', false, 1.0);
    wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), 1.0, false);
    wp_enqueue_script('bootstrap', NODE . 'bootstrap/dist/js/bootstrap.min.js', array('jquery'), 1.0, false);


    wp_enqueue_style('slick-css', PLUGINS . 'slick/slick.css', false, 1.0);
    wp_enqueue_style('slick-theme-css', PLUGINS . 'slick/slick-theme.css', false, 1.0);
    wp_enqueue_script('slick-js', PLUGINS . 'slick/slick.min.js', array('jquery'), 1.0, true);

    wp_enqueue_style('lightbox-css', PLUGINS . 'lightbox/src/css/lightbox.css', false, 1.0);
    wp_enqueue_script('lightbox-js', PLUGINS . 'lightbox/src/js/lightbox.js', array('jquery'), 1.0, true);

    wp_enqueue_style('app-style', STYLES . 'styles.css', false, 1.0);
    $object = array(
        'wp_ajax' => admin_url( 'admin-ajax.php' ),
    );


    wp_enqueue_script('app-scripts');
    wp_localize_script('app-scripts', 'JSObject', $object);
    wp_enqueue_script('app-scripts-setup', SCRIPTS . 'setup.js', array('jquery'), 1.0, true);
    wp_enqueue_script('app-scripts', SCRIPTS . 'scripts.js', array('jquery'), 1.0, true);

    wp_enqueue_script('wow', NODE . 'wow.js/dist/wow.min.js', array('jquery'), 1.0, false);
	if ((LANG !== NULL) && (LANG !== 'he')) {
		wp_enqueue_style('ltr-style', STYLES. 'ltr.css', false, 1.0);
	}
    wp_enqueue_style('primary-style', get_stylesheet_uri(), '', '1.0');

    //wp_enqueue_script('scroll-scripts', PLUGINS . 'scrollbar/jquery.mCustomScrollbar.concat.min.js', array('jquery'), 1.0, true);
}

add_action('wp_enqueue_scripts', 'scriptsInit');

function admin_style(){

    $cm_settings['codeEditor'] = wp_enqueue_code_editor(array('type' => 'text/css'));
    wp_localize_script('jquery', 'cm_settings', $cm_settings);

    wp_enqueue_script('wp-theme-plugin-editor');
    wp_enqueue_style('wp-codemirror');

    wp_enqueue_script('admin-custom', SCRIPTS . 'admin.js', array('jquery'), 1.0, false);
    wp_enqueue_style('admin-styles', STYLES . 'admin.css');
}

add_action('admin_enqueue_scripts', 'admin_style');

function registerMenus()
{
    register_nav_menus(
        array(
            'footer-menu' => __('Footer Menu', TEXTDOMAIN),
            'header-menu' => __('Header Menu', TEXTDOMAIN),
			'footer-links-menu' => __('Footer Links Menu', TEXTDOMAIN),
		)
    );
}

add_action('init', 'registerMenus');

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'הגדרות נוספות',
        'menu_title' => 'הגדרות נוספות',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

add_theme_support( 'post-thumbnails' );

add_filter('acf/settings/save_json', 'acf_json_save_point');
//acf json init
function acf_json_save_point( $path ) {
    $path = get_stylesheet_directory() . '/acf';
    return $path;
}


add_filter('acf/settings/load_json', 'acf_json_load_point');
function acf_json_load_point( $paths ) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/acf';
    return $paths;
}
//WOO
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
//Product page
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);


add_action('woo_custom_breadcrumb', 'woocommerce_breadcrumb');
add_filter( 'woocommerce_breadcrumb_defaults', 'wps_breadcrumb_delimiter' );
function wps_breadcrumb_delimiter( $defaults ) {
	$defaults['delimiter'] = ' > ';
	return $defaults;
}
/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
	// $cols contains the current number of products per page based on the value stored on Options –> Reading
	// Return the number of products you wanna show per page.
	$cols = 9;
	return $cols;
}
add_filter( 'nav_menu_css_class', 'filter_function_name_8591', 10, 4 );
function filter_function_name_8591( $classes, $item, $args, $depth ){
    if ($depth == 0) {
        $classes[] = 'first-sub-menu';
    } elseif ($depth == 1) {
        $classes[] = 'second-sub-menu';
    }
    return $classes;
}
function add_favicon() {
    echo '<link rel="shortcut icon" type="image/jpeg" href="'.get_template_directory_uri().'/assets/img/favicon.png" />';
}
add_action('wp_head', 'add_favicon');

require_once "inc/options.php";
require_once "inc/custom-types.php";
require_once "inc/users.php";
require_once "inc/ajax_calls.php";
