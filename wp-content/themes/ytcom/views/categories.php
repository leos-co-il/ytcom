<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$cats = get_terms([
    'taxonomy' => 'product_cat',
    'hide_empty' => false,
    'number' => 6,
]);
$published_posts = wp_count_terms('product_cat');
?>

<article class="page-body">
    <?php get_template_part('views/partials/content', 'page_top',
        [
            'slider' => $fields['page_slider'],
            'top_content' => $fields['top_content'],
        ]); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1 class="base-title"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-9 col-md-10 col-12">
                <div class="base-output text-center">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="cats-body-back">
        <?php if ($cats) : ?>
            <div class="container">
                <div class="row justify-content-center align-items-stretch put-here-posts">
                    <?php foreach ($cats as $i => $cat) : ?>
                        <?php get_template_part('views/partials/card', 'category', [
                            'cat' => $cat,
                        ]); ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if ($published_posts && $published_posts > 6) : ?>
                <div class="container mt-4">
                    <div class="row justify-content-center">
                        <div class="col-auto">
                            <div class="more-link load-more-posts" data-type="product_cat" data-count="<?= $published_posts; ?>">
                                <?= lang_text(['he' => 'טען עוד קטגוריות', 'en' => 'Load more categories'], 'he'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php get_template_part('views/partials/repeat', 'clients'); ?>
    </div>
</article>
<div class="mb-banner">
    <?php get_template_part('views/partials/repeat', 'banner',
    [
        'content' => $fields['banner_text'],
        'img' => $fields['banner_img'],
    ]); ?>
</div>
<?php get_footer(); ?>
