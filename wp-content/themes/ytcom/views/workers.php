<?php
/*
Template Name: צוות
*/

get_header();
$fields = get_fields();
?>
<article class="article-page-body page-body">
    <?php get_template_part('views/partials/content', 'page_top',
		[
			'slider' => $fields['page_slider'],
			'top_content' => $fields['top_content'],
		]); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1 class="base-title"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-9 col-md-10 col-12">
                <div class="base-output text-center">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
    <?php if ($fields['worker_item']) : $all_workers = count($fields['worker_item']); ?>
        <div class="workers-output">
            <div class="container">
                <div class="row justify-content-center align-items-stretch put-here-posts">
                    <?php foreach ($fields['worker_item'] as $i => $worker) :  ?>
                        <?php if ($i < 3) : get_template_part('views/partials/card', 'worker', [
                            'worker' => $worker,
                            'num' => $i,
                        ]); endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php if ($all_workers > 3) : ?>
            <div class="container mb-3">
                <div class="row justify-content-center">
                    <div class="col-auto">
                        <div class="more-link load-more-posts" data-type="worker" data-page="<?= get_queried_object_id(); ?>">
                            <?= lang_text(['he' => 'טען עוד מהצוות', 'en' => 'Load more from the team'], 'he'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;
    endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'banner',
    [
        'content' => $fields['banner_text'],
        'img' => $fields['banner_img'],
    ]);
if ($fields['single_slider_seo']) {
    get_template_part('views/partials/content', 'slider',
        [
            'content' => $fields['single_slider_seo'],
            'img' => $fields['slider_img'],
        ]);
}
get_footer(); ?>
