<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<?php if ($fields['home_slider']) : ?>
    <section class="main-block">
        <div class="main-slider" dir="rtl">
            <?php foreach ($fields['home_slider'] as $slide) : ?>
                <?php if ($slide['acf_fc_layout'] === 'post_img') : ?>
                    <div>
                        <div class="main-slide-item"
                            <?php if ($slide_img = $slide['img_url']) : ?>
                                style="background-image: url('<?= $slide_img['url']; ?>')"
                            <?php endif; ?>>
                        </div>
                    </div>
                <?php endif;
                if ($slide['acf_fc_layout'] === 'post_video') : ?>
                    <div>
                        <div class="main-slide-item main-video-item">
                            <?php if ($video = $slide['video_url']) : ?>
                                <video muted autoplay="autoplay" loop="loop">
                                    <source src="<?= $video['url']; ?>" type="video/mp4">
                                </video>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif;
if ($fields['h_about_text']) : ?>
    <section class="home-about-block">
        <div class="container">
            <div class="row justify-content-between align-items-center mb-5">
                <div class="col-12">
                    <div class="content-about-back">
                        <div class="base-output base-output-white mb-3">
                            <?= $fields['h_about_text']; ?>
                        </div>
                        <?php if ($fields['h_about_link']) : ?>
                            <a href="<?= $fields['h_about_link']['url'];?>" class="block-link about-home-link">
                                <?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
                                    ? $fields['h_about_link']['title'] : lang_text(['he' => 'עוד עלינו', 'en' => 'More about us'], 'he');
                                ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;
if ($fields['h_service_item']) : ?>
    <section class="home-services slider-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <h2 class="base-title">
                        <?= $fields['h_services_title']; ?>
                    </h2>
                </div>
            </div>
            <div class="row justify-content-center align-items-stretch arrows-slider big-arrows services-arrows">
                <div class="col-xl-11 col-12 mb-2">
                    <div class="product-slider" dir="rtl">
                        <?php foreach ($fields['h_service_item'] as $service) : ?>
                            <div class="p-2">
                                <a class="service-item" href="<?= $service['item_link']; ?>">
                                    <span class="service-icon-wrap">
                                        <?php if ($service['item_icon']) : ?>
                                            <span class="serv-icon-item">
                                                <img src="<?= $service['item_icon']['url']; ?>">
                                            </span>
                                        <?php endif; ?>
                                    </span>
                                    <h3 class="serv-item-title"><?= $service['item_title']; ?></h3>
                                    <p class="serv-item-text"><?= $service['item_text']; ?></p>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php if ($fields['h_services_link']) : ?>
                <div class="row justify-content-center pt-3">
                    <div class="col-auto">
                        <a href="<?= $fields['h_services_link']['url'];?>" class="block-link">
                            <?= (isset($fields['h_services_link']['title']) && $fields['h_services_link']['title'])
                                ? $fields['h_services_link']['title'] : lang_text(['he' => 'שירותים נוספים', 'en' => 'More services'], 'he');
                            ?>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif;
if ($fields['h_products']) : ?>
    <section class="home-products slider-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <h2 class="base-title">
                        <?= $fields['h_products_title']; ?>
                    </h2>
                </div>
            </div>
            <div class="row justify-content-center align-items-stretch arrows-slider big-arrows">
                <div class="col-12">
                    <div class="product-slider" dir="rtl">
                        <?php foreach ($fields['h_products'] as $_p){
                            echo '<div class="p-2">';
                            setup_postdata($GLOBALS['post'] =& $_p);
                            wc_get_template_part( 'content', 'product' );
                            echo '</div>';
                        } wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
            <?php if ($fields['h_products_link']) : ?>
                <div class="row justify-content-center mt-3">
                    <div class="col-auto">
                        <a href="<?= $fields['h_products_link']['url'];?>" class="block-link">
                            <?= (isset($fields['h_products_link']['title']) && $fields['h_products_link']['title'])
                                ? $fields['h_products_link']['title'] : lang_text(['he' => 'מוצרים נוספים', 'en' => 'More products'], 'he');
                            ?>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif;
if ($fields['h_cats']) : ?>
    <section class="home-cats">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <h2 class="base-title">
                        <?= $fields['h_cats_title']; ?>
                    </h2>
                </div>
            </div>
            <div class="row justify-content-center align-items-stretch row-cats">
                <?php foreach ($fields['h_cats'] as $cat) : ?>
                    <div class="col-xl-4 col-md-6 col-12 cat-col">
                        <div class="cat-item-img" <?php $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                        $image = wp_get_attachment_url( $thumbnail_id );
                        if ($image) : ?>
                            style="background-image: url('<?= $image; ?>')"
                        <?php endif; ?>>
                            <span class="cat-item-overlay"></span>
                            <a class="cat-item-info" href="<?= $link = get_category_link($cat); ?>">
                                <span class="base-title-white">
                                    <?= $cat->name; ?>
                                </span>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif;
get_template_part('views/partials/repeat', 'clients');
get_template_part('views/partials/repeat', 'banner',
    [
        'content' => $fields['banner_text'],
        'img' => $fields['banner_img'],
    ]);
if ($fields['single_slider_seo']) {
    get_template_part('views/partials/content', 'slider',
        [
            'content' => $fields['single_slider_seo'],
            'img' => $fields['slider_img'],
        ]);
}
get_footer(); ?>
