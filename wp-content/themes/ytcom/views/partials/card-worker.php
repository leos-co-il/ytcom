<?php if(isset($args['worker']) && $args['worker']) : ?>
    <div class="col-xl-4 col-sm-6 col-12 worker-col">
        <div class="worker-item more-card" data-id="<?= $args['num']; ?>">
            <div class="worker-item" <?php if ($args['worker']['worker_img']) : ?>
                style="background-image: url('<?= $args['worker']['worker_img']['url']; ?>')"
            <?php endif; ?>>
                <div class="worker-content">
                    <div class="worker-title-wrap">
                        <h3 class="worker-name">
                            <?= $args['worker']['worker_name']; ?>
                        </h3>
                    </div>
                </div>
                <span class="worker-trigger">
                    <img src="<?= ICONS ?>plus.png" alt="read-more">
                </span>
                <span class="worker-close">
                        <img src="<?= ICONS ?>minus.png" alt="close-popup">
                    </span>
                <div class="worker-content-appear">
                    <h3 class="worker-name">
                        <?= $args['worker']['worker_name']; ?>
                    </h3>
                    <div class="base-output">
                        <?= $args['worker']['worker_text']; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
