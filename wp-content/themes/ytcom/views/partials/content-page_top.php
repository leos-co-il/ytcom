<?php
$slider_top = (isset($args['slider']) && $args['slider']) ? $args['slider'] : opt('page_slider');
if ($slider_top) : ?>
    <div class="top-slider arrows-slider arrows-slider-base">
        <div class="base-slider" dir="rtl">
            <?php foreach ($slider_top as $content) : ?>
                <div class="slider-top-item">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-xl-8 col-lg-10 col-12">
                                <div class="slider-top-title">
                                    <?= $content['title']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif;
$content_promo = isset($args['top_content']) && ($args['top_content']) ? $args['top_content'] : opt('top_content'); ?>
<div class="top-page-content pt-4">
    <?php if ($content_promo) : ?>
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-11 col-12">
                    <div class="base-output">
                        <?= $content_promo; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;
    if ( function_exists('yoast_breadcrumb')) : ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

