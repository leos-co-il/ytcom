<?php if (isset($args['content']) && ($args['content'])) :
    $slider_img = isset($args['img']) ? ($args['img']) : ''; ?>
    <div class="base-slider-block arrows-slider arrows-slider-base">
        <div class="container">
            <div class="row justify-content-between align-items-stretch">
                <div class="<?= $slider_img ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?> slider-col-content">
                    <div class="base-slider" dir="rtl">
                        <?php foreach ($args['content'] as $content) : ?>
                            <div class="slider-base-item">
                                <div class="base-output base-output-white">
                                    <?= $content['content']; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php if ($slider_img) : ?>
                    <div class="col-xl-5 col-lg-6 col-12 slider-img-col justify-content-end align-items-center">
                        <img src="<?= $slider_img['url']; ?>" class="img-slider">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
