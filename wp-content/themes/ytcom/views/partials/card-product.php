<?php

if (!isset($args['products']))
    return;
foreach ($args['products'] as $_p){
    echo '<div class="col-xl-4 col-sm-6 col-12 mb-4 product-item-col prod-loop-col">';
    setup_postdata($GLOBALS['post'] =& $_p);
    wc_get_template_part( 'content', 'product' );
    echo '</div>';
}
wp_reset_postdata();
?>
