<?php
$reviews = (isset($args['content']) && $args['content']) ? $args['content'] : opt('review_item');
$title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('review_title');
$logo = opt('logo');
if ($reviews) : ?>
    <section class="reviews-part py-5">
        <div class="container container-revs">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <h2 class="base-title">
                        <?= $title ? $title : lang_text(['he' => 'המלצות של לקוחות', 'en' => 'Reviews'], 'he'); ?>
                    </h2>
                </div>
            </div>
            <div class="row justify-content-center arrows-slider review-arrows">
                <div class="col-lg-10 col-11">
                    <div class="base-slider revs-slider" dir="rtl">
                        <?php foreach ($reviews as $x => $review) : ?>
                            <div class="review-slide">
                                <div class="review-item">
                                    <?php $logo_here = (isset($review['rev_logo']) && $review['rev_logo']) ? $review['rev_logo']: $logo;
                                    if ($logo_here) : ?>
                                        <div class="row justify-content-center">
                                            <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 col-11 mb-3">
                                                <a href="/" class="logo">
                                                    <img src="<?= $logo_here['url'] ?>" alt="logo">
                                                </a>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <h3 class="middle-title"><?= $review['rev_name']; ?></h3>
                                    <div class="base-output text-center">
                                        <?= $review['rev_text']; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
