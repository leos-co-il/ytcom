<?php if (isset($args['cat']) && $args['cat']) : ?>
    <div class="col-sm-6 col-12 cat-col more-card" data-id="<?= $args['cat']->term_id; ?>">
        <div class="cat-item-img" <?php $thumbnail_id = get_term_meta( $args['cat']->term_id, 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        if ($image) : ?>
            style="background-image: url('<?= $image; ?>')"
        <?php endif; ?>>
            <span class="cat-item-overlay"></span>
		    <a class="cat-item-info" href="<?= $link = get_category_link($args['cat']); ?>">
				<span class="base-title-white">
					<?= $args['cat']->name; ?>
				</span>
			</a>
        </div>
    </div>
<?php endif; ?>
