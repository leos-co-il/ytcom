<?php if ($clients = opt('partners')) :
    $title = opt('partners_title'); ?>
    <section class="clients-block">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <h2 class="base-title">
                        <?= $title ? $title : lang_text(['he' => 'מבין לקוחותינו/', 'en' => 'Our customers /'], 'he'); ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="clients-line">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 arrows-slider">
                        <div class="partners-slider" dir="rtl">
                            <?php foreach ($clients as $partner) : ?>
                                <div>
                                    <div class="client-logo">
                                        <img src="<?= $partner['url']; ?>" alt="customer-logo">
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
