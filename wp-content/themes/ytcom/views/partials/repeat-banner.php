<?php
$base_img = opt('basic_banner_img');
$banner_img = (isset($args['img']) && $args['img']) ? $args['img'] : $base_img;
$banner_text = (isset($args['content']) && $args['content']) ? $args['content'] : opt('basic_banner_text');
if($banner_img): ?>
    <section class="banner-block" <?php if ($banner_img) : ?> style="background-image: url('<?= $banner_img['url']; ?>')" <?php endif; ?>>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10 col-12">
                    <h2 class="banner-title">
                        <?= $banner_text; ?>
                    </h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>