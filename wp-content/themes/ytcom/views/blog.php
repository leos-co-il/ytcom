<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 6,
	'post_type' => 'post',
	'suppress_filters' => false,
]);
$published_posts = count(get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'suppress_filters' => false
]));
?>

<article class="article-page-body page-body">
    <?php get_template_part('views/partials/content', 'page_top',
        [
            'slider' => $fields['page_slider'],
            'top_content' => $fields['top_content'],
        ]); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1 class="base-title"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-9 col-md-10 col-12">
                <div class="base-output text-center">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="cats-body-back">
        <?php if ($posts->have_posts()) : ?>
            <div class="container">
                <div class="row justify-content-center align-items-stretch put-here-posts">
                    <?php foreach ($posts->posts as $i => $post) {
                        get_template_part('views/partials/card', 'post', [
                            'post' => $post,
                        ]);
                    } ?>
                </div>
            </div>
        <?php endif;
        if ($published_posts && $published_posts > 6) : ?>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-auto">
                        <div class="more-link load-more-posts" data-type="post">
                            <?= lang_text(['he' => 'טען עוד מאמרים', 'en' => 'Load more posts'], 'he'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</article>
<?php
get_template_part('views/partials/repeat', 'reviews',
    [
        'content' => $fields['review_item'],
        'title' => $fields['reviews_title'],
    ]);
get_template_part('views/partials/repeat', 'banner',
    [
        'content' => $fields['banner_text'],
        'img' => $fields['banner_img'],
    ]);
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
get_footer(); ?>
