<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$fax = opt('fax');
$map = opt('map_image');
?>
<article class="page-body">
	<div class="contact-page-body">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h1 class="base-title"><?php the_title(); ?></h1>
				</div>
				<div class="col-12">
					<div class="base-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-12 mb-5">
					<div class="form-wrapper">
						<div class="row justify-content-center align-items-stretch">
							<div class="col-lg-6 col-12 col-contact-pad">
								<div class="contact-col h-100">
									<?php if ($fields['contact_contacts_title']) : ?>
										<h3 class="post-form-subtitle"><?= $fields['contact_contacts_title']; ?></h3>
									<?php endif;
									if ($tel) : ?>
										<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
											<div class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-tel.png">
											</div>
											<div class="contact-info">
												<h3 class="contact-type"><?= $tel; ?></h3>
											</div>
										</a>
									<?php endif; ?>
									<?php if ($mail) : ?>
										<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
											<div class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-mail.png">
											</div>
											<div class="contact-info">
												<h3 class="contact-type"><?= $mail; ?></h3>
											</div>
										</a>
									<?php endif;
									if ($fax) : ?>
										<div class="contact-item wow flipInX" data-wow-delay="0.6s">
											<div class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-fax.png">
											</div>
											<div class="contact-info">
												<h3 class="contact-type"><?= $fax; ?></h3>
											</div>
										</div>
									<?php endif;
									if ($address) : ?>
										<a href="https://waze.com/ul?q=<?= $address; ?>" class="contact-item wow flipInX" data-wow-delay="0.8s">
											<div class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-geo.png">
											</div>
											<div class="contact-info">
												<h3 class="contact-type"><?= $address; ?></h3>
											</div>
										</a>
									<?php endif; ?>
								</div>
							</div>
							<div class="col-lg-6 col-12 col-contact-pad">
								<div class="form-wrapper-inside h-100">
									<?php if ($fields['contact_form_title']) : ?>
										<h3 class="post-form-subtitle"><?= $fields['contact_form_title']; ?></h3>
									<?php endif;
									getForm('54'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($map) : ?>
		<a class="map-image" href="<?= $map['url']; ?>" style="background-image: url('<?= $map['url']; ?>')"
		data-lightbox="map"></a>
	<?php endif; ?>
</article>
<?php get_footer(); ?>
