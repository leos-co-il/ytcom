<?php get_header(); ?>
<article class="article-page-body page-body">
    <div class="container pt-3">
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1 class="base-title"><?php the_title(); ?></h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="base-output">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</article>
<div class="margin-foo">
    <?php get_footer(); ?>
</div>
