<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$save = lang_text(['he' => 'הורידו את הקובץ', 'en' => 'Download file'], 'he');
$video_title = lang_text(['he' => 'לצפות בסרט', 'en' => 'To watch a video'], 'he');
$post_gallery = $fields['post_gallery'];
?>
<article class="article-page-body">
    <?php
    $slider_top = $fields['page_slider'] ? $fields['page_slider'] : opt('page_slider');
    if ($slider_top) : ?>
        <div class="top-slider arrows-slider arrows-slider-base">
            <div class="base-slider" dir="rtl">
                <?php foreach ($slider_top as $content) : ?>
                    <div class="slider-top-item">
                        <div class="container">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-xl-8 col-lg-10 col-12">
                                    <div class="slider-top-title">
                                        <?= $content['title']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="top-page-content pt-4">
        <?php if ( function_exists('yoast_breadcrumb')) : ?>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="container pt-4">
		<div class="row justify-content-between align-items-stretch">
			<div class="col-lg-7 col-12 d-flex flex-column align-items-start">
                <h1 class="base-title base-title-post"><?php the_title(); ?></h1>
                <div class="base-output page-output">
					<?php the_content(); ?>
				</div>
				<?php if ($fields['post_file']) : ?>
					<a class="link-post-save" download href="<?= $fields['post_file']['url']; ?>">
						<span class="file-icon-wrap">
							<img src="<?= ICONS ?>download.png">
						</span>
						<span class="file-save-text">
							<?= (isset($fields['post_file']['title']) && $fields['post_file']['title']) ?
								$fields['post_file']['title'] : $save; ?>
						</span>
					</a>
				<?php endif; ?>
				<div class="trigger-wrap">
					<a class="social-trigger">
						<span class="social-item-share share-soc-item">
                            <i class="fas fa-share-alt"></i>
                        </span>
						<span class="social-item-text">
							<?= lang_text(['he' => 'שתפו את המאמר', 'en' => 'Share the article'], 'he'); ?>
						</span>
					</a>
					<div class="all-socials item-socials" id="show-socials">
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="socials-wrap social-item-share">
                            <i class="fab fa-whatsapp"></i>
                        </a>
						<a class="socials-wrap social-item-share" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $current_id; ?>"
						   target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
						<a class="socials-wrap social-item-share" href="mailto:?&subject=&body=<?= $current_id; ?>">
                            <i class="far fa-envelope"></i>
                        </a>
					</div>
				</div>
			</div>
			<div class="col-xl-5 col-lg-5 col-12 sticky-form-col">
				<div class="sticky-form">
					<?php get_template_part('views/partials/repeat', 'form'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php if ($post_gallery) : $chunks = array_chunk($post_gallery, 3); ?>
	<div class="post-gallery-wrap mb-5 mt-3">
		<div class="container">
            <div class="row justify-content-center align-items-stretch">
                <?php foreach ($chunks as $chunk) : ?>
                    <div class="col-lg-6 col-12 col-gallery-item">
                        <?php foreach ($chunk as $item) : ?>
                            <div class="gallery-item-post">
                                <?php if ($item['acf_fc_layout'] === 'post_img') : $img = $item['img_url'] ? $item['img_url']['url'] : ''; if ($img) : ?>
                                    <a href="<?= $img; ?>" data-lightbox="images" style="background-image: url('<?= $img; ?>')" class="image-item-gal"></a>
                                <?php endif; endif;
                                if ($item['acf_fc_layout'] === 'post_video') : $img = $item['video_url'] ? getYoutubeThumb($item['video_url']) : '';
                                    if ($item['video_url']) : ?>
                                        <div data-video="<?= getYoutubeId($item['video_url']); ?>" style="background-image: url('<?= $img; ?>')"
                                             class="play-button image-item-gal"></div>
                                    <?php endif; endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
	</div>
    <div class="video-modal">
        <div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body" id="iframe-wrapper"></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="close-icon">
                            <img src="<?= ICONS ?>close.png">
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="reviews-back">
    <?php get_template_part('views/partials/repeat', 'reviews',
    [
        'content' => $fields['review_item'],
        'title' => $fields['reviews_title'],
    ]); ?>
</div>
<?php
$text = lang_text(['he' => 'מאמרים נוספים', 'en' => 'More articles'], 'he');
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 3,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="home-posts my-5">
		<div class="container">
			<div class="row justify-content-center">
                <div class="col-auto">
                    <h2 class="base-title">
                        <?= $fields['same_title'] ? $fields['same_title'] : $text; ?>
                    </h2>
                </div>
			</div>
		</div>
		<div class="posts-output">
            <div class="container">
                <div class="row justify-content-center align-items-stretch">
                    <?php foreach ($samePosts as $i => $post) {
                        get_template_part('views/partials/card', 'post', [
                            'post' => $post,
                        ]); } ?>
                </div>
            </div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'banner',
    [
        'content' => $fields['banner_text'],
        'img' => $fields['banner_img'],
    ]);
if ($fields['single_slider_seo']) : ?>
	<?php get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $fields['single_slider_seo'],
		]);
    get_footer(); ?>
<?php else : ?>
    <div class="margin-foo">
        <?php get_footer(); ?>
    </div>
<?php endif; ?>
