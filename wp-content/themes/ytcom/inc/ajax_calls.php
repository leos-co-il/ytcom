<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function()
{
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : 'post';
    $count_cat = (isset($_REQUEST['count'])) ? $_REQUEST['count'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$id_page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
	$quantity = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	$ids = explode(',', $ids_string);
	$cat = ($type === 'product') ? 'product_cat' : 'category';
	$html = '';
	$result['html'] = '';
	$result['quantity'] = '';
	if ($type === 'post') {
        $query = new WP_Query([
            'post_type' => $type,
            'posts_per_page' => 3,
            'post__not_in' => $ids,
            'tax_query' => $id_term ? [
                [
                    'taxonomy' => $cat,
                    'field' => 'term_id',
                    'terms' => $id_term,
                ]
            ] : '',
        ]);
		if ($query->have_posts()) {
			foreach ($query->posts as $item) {
				$html = load_template_part('views/partials/card', 'post', [
					'post' => $item,
				]);
				$result['html'] .= $html;
			}
			if (wp_count_posts($type)->publish <= ($quantity + 3)) {
				$result['quantity'] = true;
			}
		}
	} elseif (($type === 'worker') && $id_page) {
		$query = get_field('worker_item', $id_page);
		foreach ($query as $i => $item) {
			if (!in_array($i, $ids)) {
				$html = load_template_part('views/partials/card', 'worker', [
					'worker' => $item,
					'num' => $i,
				]);
				$result['html'] .= $html;
			}
		}
		if (count($query) <= ($quantity + 3)) {
			$result['quantity'] = true;
		}
	} elseif ($type === 'product_cat') {
        $cats = get_terms([
            'taxonomy' => $type,
            'hide_empty' => false,
            'exclude' => $ids,
            'number' => 2,
        ]);
        foreach ($cats as $item) {
            $html = load_template_part('views/partials/card', 'category', [
                'cat' => $item,
            ]);
            $result['html'] .= $html;
        }
        if ($count_cat <= ($quantity + 2)) {
            $result['quantity'] = true;
        }
    }
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}

function add_product_to_cart() {

	$product_id = isset($_REQUEST['product_id']) ?  intval($_REQUEST['product_id']) : null;
	$quantity = isset($_REQUEST['quantity']) ? intval($_REQUEST['quantity']) : null;
	$custom_data = [];
	$_product = wc_get_product( $product_id );


	$cart = WC()->cart;

	if($_POST['delete'] === 'true'){
		$cart->remove_cart_item($product_id);
	}else{
		$cart->add_to_cart( $product_id, $quantity, '0', array(), $custom_data );
	}

	$result['type'] = "success";

//	wc_get_template( 'cart/mini-cart.php' );

	die();
}

add_action('wp_ajax_add_product_to_cart', 'add_product_to_cart');
add_action('wp_ajax_nopriv_add_product_to_cart', 'add_product_to_cart');


add_action('wp_ajax_nopriv_update_cart_count', 'update_cart_count');
add_action('wp_ajax_update_cart_count', 'update_cart_count');
function update_cart_count(){
	global $woocommerce;
	echo $woocommerce->cart->cart_contents_count;
}

function loadmore() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$ids = explode(',', $ids_string);
	$paged = ! empty( $_POST[ 'paged' ] ) ? $_POST[ 'paged' ] : 1;
	$paged++;

    $query = new WP_Query([
        'paged' => $paged,
        'post_type' => 'product',
        'posts_per_page' => 3,
        'post__not_in' => $ids,
    ]);
	$html = '';
	$result['html'] = '';
    $html = load_template_part('views/partials/card', 'product', [
        'products' => $query->posts,
    ]);
    $result['html'] = $html;
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}

add_action( 'wp_ajax_loadmore', 'loadmore' );
add_action( 'wp_ajax_nopriv_loadmore', 'loadmore' );