<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
$post_id = $product->get_id();
$fields = get_fields();
$category = get_the_terms($post_id, 'product_cat');
$ids = [];
if ($category) {
    foreach ($category as $cat_id) {
        $ids[] = $cat_id->term_id;
    }
}
$post_link = get_the_permalink();
$product_thumb = wp_get_attachment_image_url( get_post_thumbnail_id($post_id), 'full' );;
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
    foreach ($product_gallery as $_item){
        $product_gallery_images[] = wp_get_attachment_image_url($_item, 'large', '');
    }
}
$info_content = get_the_content();
$related_products_base = $fields['same_prods'];
$related_products_1 = $related_products_base? $related_products_base : get_posts([
    'post_type' => 'product',
    'exclude' => $post_id,
    'numberposts' => 4,
    'tax_query' => [
        [
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $ids,
        ],
    ],
]);
$related_products_2 = $fields['same_prods_2'];
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'page-body product-page-article', $product ); ?>>
    <?php get_template_part('views/partials/content', 'page_top',
        [
            'slider' => $fields['page_slider'],
            'top_content' => $fields['top_content'],
        ]); ?>
    <div class="container product-body">
        <div class="row">
            <div class="col-auto hidden-title">
                <h2 class="base-title"><?php the_title(); ?></h2>
            </div>
        </div>
        <div class="row justify-content-between">
            <div class="col-xl-5 col-lg-6 d-flex flex-column align-items-start">
                <div class="summary entry-summary">
                    <?php
                    /**
                     * Hook: woocommerce_single_product_summary.
                     *
                     * @hooked woocommerce_template_single_title - 5
                     * @hooked woocommerce_template_single_rating - 10
                     * @hooked woocommerce_template_single_price - 10
                     * @hooked woocommerce_template_single_excerpt - 20
                     * @hooked woocommerce_template_single_add_to_cart - 30
                     * @hooked woocommerce_template_single_meta - 40
                     * @hooked woocommerce_template_single_sharing - 50
                     * @hooked WC_Structured_Data::generate_product_data() - 60
                     */
                    do_action( 'woocommerce_single_product_summary' ); ?>
                    <div class="base-output">
                        <?php the_content(); ?>
                    </div>
                    <?php if ($fields['prod_video']) : ?>
                        <div class="video-trigger-wrap mb-4">
						<span class="play-button" data-video="<?= getYoutubeId($fields['prod_video']['url']); ?>">
							<img src="<?=ICONS ?>play.png" alt="play-button">
						</span>
                            <?= (isset($fields['prod_video']['title']) && $fields['prod_video']['title']) ? $fields['prod_video']['title'] :
                            lang_text(['he' => 'צפו בסרטון', 'en' => 'Watch the video'], 'he'); ?>
                        </div>
                        <div class="video-modal">
                            <div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body" id="iframe-wrapper"></div>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" class="close-icon">
                                                <img src="<?= ICONS ?>close.png">
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif;
                    if ($fields['prod_files']) : ?>
                        <div class="d-flex flex-column align-items-start">
                            <?php foreach ($fields['prod_files'] as $file) : ?>
                                <a class="link-post-save mb-2" download href="<?= $file['file_link']['url']; ?>">
                                    <span class="file-icon-wrap">
                                        <img src="<?= ICONS ?>download.png">
                                    </span>
                                    <span class="file-save-text">
                                        <?= (isset($file['file_link']['title']) && $file['file_link']['title']) ?
                                            $file['file_link']['title'] : lang_text(['he' => 'הורד את הקובץ', 'en' => 'Download the file'], 'he');; ?>
                                    </span>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-6 gallery-slider-wrap arrows-slider review-arrows">
                <div class="gallery-slider" dir="rtl">
                    <?php if($product_thumb): ?>
                        <div class="p-2">
                            <a class="big-slider-item" style="background-image: url('<?= $product_thumb; ?>')"
                               href="<?= $product_thumb; ?>" data-lightbox="images"></a>
                        </div>
                    <?php endif;
                    foreach ($product_gallery_images as $img): ?>
                        <div class="p-2">
                            <a class="big-slider-item" style="background-image: url('<?= $img; ?>')"
                               href="<?= $img; ?>" data-lightbox="images">
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="thumbs" dir="rtl">
                    <?php if($product_thumb): ?>
                        <div class="p-2">
                            <a class="thumb-item" style="background-image: url('<?= $product_thumb; ?>')"
                               href="<?= $product_thumb; ?>" data-lightbox="images-small"></a>
                        </div>
                    <?php endif;
                    foreach ($product_gallery_images as $img): ?>
                        <div class="p-2">
                            <a class="thumb-item" style="background-image: url('<?= $img; ?>')"
                               href="<?= $img; ?>" data-lightbox="images-small">
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="product-share-line">
        <div class="container-fluid">
            <div class="row justify-content-end">
                <div class="col-auto">
                    <div class="trigger-wrap">
                        <a class="social-trigger">
				<span class="social-item-text">
                    <?= lang_text(['he' => 'שתפו את המאמר', 'en' => 'Share the article'], 'he'); ?>
				</span>
                            <span class="social-item m-0">
					<i class="fas fa-share-alt"></i>
				</span>
                        </a>
                        <div class="all-socials item-socials" id="show-socials">
                            <a class="socials-wrap social-item" href="mailto:?&subject=&body=<?= $post_link; ?>">
                                <i class="fas fa-envelope"></i>
                            </a>
                            <a class="socials-wrap social-item" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>"
                               target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $post_link; ?>" target="_blank"
                               class="socials-wrap social-item">
                                <i class="fab fa-whatsapp"></i>
                            </a>
                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $post_link; ?>&title=&summary=&source="
                               target="_blank"
                               class="socials-wrap social-item">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="add-cart-line line-form-prod">
        <?php if ( ! $product->is_purchasable() ) :
            $form_title = opt('prod_form_title'); $form_subtitle = opt('prod_form_subtitle'); ?>
            <div class="container form-product prod-form-wrap" data-id="<?= $post_id; ?>" data-link="<?= $post_link;?>">
                <div class="row align-items-center justify-content-center mb-3">
                    <?php if ($form_title || $form_subtitle) : ?>
                        <div class="col-12">
                            <h2 class="foo-form-title text-center">
                                <?= $form_title; ?>
                            </h2>
                            <h3 class="foo-form-subtitle text-center">
                                <?= $form_subtitle; ?>
                            </h3>
                        </div>
                    <?php endif; ?>
                </div>
                <?php getForm('56'); ?>
            </div>
        <?php endif;

        echo wc_get_stock_html( $product ); // WPCS: XSS ok.

        if ( $product->is_purchasable() && $product->is_in_stock() ) : ?>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-11 col-12">
                        <div class="add-to-cart-single-item">
                            <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
                            <form class="cart row" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>"
                                  method="post" enctype='multipart/form-data'>
                                <div class="col-lg-auto col-12 qty-col-wrap">
                                    <div class="wrapper-product-wish">
                                        <div class="qty-wrap">
                                            <div class="plus" data-id="<?= $product->get_id() ?>">
                                                <img src="<?= ICONS ?>plus.png" alt="plus">
                                            </div>
                                            <?php

                                            do_action( 'woocommerce_before_add_to_cart_quantity' );

                                            woocommerce_quantity_input(
                                                array(
                                                    'classes' => 'qty-for-' .  $product->get_id(),
                                                    'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                                                    'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                                                    'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                                                )
                                            );

                                            do_action( 'woocommerce_after_add_to_cart_quantity' );

                                            ?>
                                            <div class="minus" data-id="<?= $product->get_id() ?>">
                                                <img src="<?= ICONS ?>minus.png" alt="minus">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price-total-wrap">
                                        <?php $sym = get_woocommerce_currency_symbol(); ?>
                                        <span>סה”כ</span>
                                        <div class="price-final-sale final-price-title" data-price_sale="<?= $product->get_sale_price(); ?>" data-sym="<?= $sym; ?>">
                                            <?= $sym.$product->get_sale_price(); ?>
                                        </div>
                                        <div class="price-final final-price-title" data-price="<?= $product->get_regular_price(); ?>" data-sym="<?= $sym; ?>">
                                            <?= $sym.$product->get_regular_price(); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
                                <div class="buy-buttons col-lg col-12">
                                    <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="add-custom single_add_to_cart_button button alt">
                                        <span>
                                            <?= lang_text(['he' => 'הוספה לסל', 'en' => 'Add to cart'], 'he'); ?>
                                        </span>
                                    </button>
                                    <a class="buy-now-custom buy-now-style" data-url="<?= site_url(); ?>"
                                       data-id="<?= $id = $product->get_id(); ?>" href="<?= site_url(); ?>/checkout/?add-to-cart=<?= $id; ?>&quantity=1'">
                                        <?= lang_text(['he' => 'קניה מהירה', 'en' => 'Buy now'], 'he'); ?>
                                    </a>
                                </div>
                                <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                            </form>

                            <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <?php
                /**
                 * Hook: woocommerce_after_single_product_summary.
                 *
                 * @hooked woocommerce_output_product_data_tabs - 10
                 * @hooked woocommerce_upsell_display - 15
                 * @hooked woocommerce_output_related_products - 20
                 */
                do_action( 'woocommerce_after_single_product_summary' );
                ?>
            </div>
        </div>
    </div>
</div>
<?php if ($related_products_1) : ?>
    <div class="related-prod">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <h2 class="base-title">
                        <?= $fields['same_prods_title'] ? $fields['same_prods_title'] :
                            lang_text(['he' => 'אביזרים ומערכות', 'en' => 'Accessories and systems'], 'he'); ?>
                    </h2>
                </div>
            </div>
            <div class="row justify-content-center align-items-stretch">
                <?php foreach ($related_products_1 as $product) : ?>
                    <div class="col-lg-3 col-md-6 col-sm-11 col-12 mb-3 prod-col">
                        <?php setup_postdata($GLOBALS['post'] =& $product);
                        wc_get_template_part( 'content', 'product' ); ?>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="reviews-back">
    <?php get_template_part('views/partials/repeat', 'reviews',
        [
            'content' => $fields['review_item'],
            'title' => $fields['reviews_title'],
        ]); ?>
</div>
<?php if ($related_products_2) : ?>
    <div class="related-prod-margin">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-auto">
                    <h2 class="base-title">
                        <?= $fields['same_prods_title_2'] ? $fields['same_prods_title_2'] :
                            lang_text(['he' => 'מוצרים דומים', 'en' => 'Related products'], 'he'); ?>
                    </h2>
                </div>
            </div>
            <div class="row justify-content-center align-items-stretch">
                <?php foreach ($related_products_1 as $product) : ?>
                    <div class="col-lg-3 col-md-6 col-sm-11 col-12 mb-3 prod-col">
                        <?php setup_postdata($GLOBALS['post'] =& $product);
                        wc_get_template_part( 'content', 'product' ); ?>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php endif;
if ($fields['single_slider_seo']) {
    get_template_part('views/partials/content', 'slider',
        [
            'img' => $fields['slider_img'],
            'content' => $fields['single_slider_seo'],
        ]);
}
do_action( 'woocommerce_after_single_product' ); ?>
