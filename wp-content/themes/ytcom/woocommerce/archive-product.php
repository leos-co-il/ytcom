<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
global $wp_query;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_pages = $wp_query->max_num_pages;
if (is_shop()) {
    $query = wc_get_page_id('shop');
    $image = has_post_thumbnail($query) ? postThumb($query) : '';
} else {
    $query_curr = get_queried_object();
    $query = $query_curr;
    $thumbnail_id = get_term_meta( $query->term_id, 'thumbnail_id', true );
    $image = wp_get_attachment_url( $thumbnail_id );
}
get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>
    <section class="top-archive">
        <?php get_template_part('views/partials/content', 'page_top',
            [
                'slider' => get_field('page_slider', $query),
                'top_content' => get_field('top_content', $query),
            ]); ?>
        <div class="title-wrap">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-auto">
                        <h1 class="base-title"><?php woocommerce_page_title(); ?></h1>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-auto">
                        <div class="base-output text-center">
                            <?php
                            /**
                             * Hook: woocommerce_archive_description.
                             *
                             * @hooked woocommerce_taxonomy_archive_description - 10
                             * @hooked woocommerce_product_archive_description - 10
                             */
                            do_action( 'woocommerce_archive_description' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<div class="shop-back">
    <div class="container cont-prods-top mb-4">
        <div class="row align-items-start">
            <div class="col-xl-3 col-lg-4">
                <?php


                /**
                 * Hook: woocommerce_sidebar.
                 *
                 * @hooked woocommerce_get_sidebar - 10
                 */
                do_action( 'woocommerce_sidebar' );

                ?>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="woo-notice">
                    <?php woocommerce_output_all_notices() ?>
                </div>
                <?php if ( woocommerce_product_loop() ) {
                    woocommerce_product_loop_start();
                    echo '<div class="row put-here-prods">';
                    if ( wc_get_loop_prop( 'total' ) ) {
                        while ( have_posts() ) {
                            the_post();

                            /**
                             * Hook: woocommerce_shop_loop.
                             */
                            do_action( 'woocommerce_shop_loop' );

                            echo '<div class="col-xl-4 col-sm-6 col-12 mb-4 product-item-col prod-loop-col">';
                            wc_get_template_part( 'content', 'product' );
                            echo '</div>';
                        }
                    }
                    echo '<div>';
                    woocommerce_product_loop_end();

                    /**
                     * Hook: woocommerce_after_shop_loop.
                     *
                     * @hooked woocommerce_pagination - 10
                     */
                    do_action( 'woocommerce_after_shop_loop' );
                } else {
                    /**
                     * Hook: woocommerce_no_products_found.
                     *
                     * @hooked wc_no_products_found - 10
                     */
                    do_action( 'woocommerce_no_products_found' );
                }
                ?>
                <?php if( $paged < $max_pages ) : ?>
                    <div class="row justify-content-center mt-4 mb-5">
                        <div class="col-auto">
                            <div id="loadmore">
                                <a href="#" data-maxpages="<?= $max_pages ?>" data-paged="<?= $paged ?>"
                                   class="more-link">
                                    <?= lang_text(['he' => 'טען עוד מוצרים', 'en' => 'Load more products'], 'he'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
get_template_part('views/partials/repeat', 'clients');
get_template_part('views/partials/repeat', 'banner',
    [
        'content' => get_field('banner_text', $query),
        'img' => get_field('banner_img', $query),
    ]);
if ($slider = get_field('single_slider_seo', $query)) {
    get_template_part('views/partials/content', 'slider',
        [
            'img' => get_field('slider_img', $query),
            'content' => $slider,
            'experience' => true
        ]);
    get_footer( 'shop' );
} else {
    echo '<div class="margin-foo">';
    get_footer( 'shop' );
    echo '</div>';
}
